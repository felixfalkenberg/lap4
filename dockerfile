# 2018 Felix Falkenberg
##################################################
# Dockerfile reference
# (https://docs.docker.com/engine/reference/builder)
#
# Docker CLI reference
# (https://docs.docker.com/engine/reference/commandline/docker/)
#
# Docker run command reference
# (https://docs.docker.com/engine/reference/commandline/run/)
##################################################
# Example: (https://spring.io/guides/gs/spring-boot-docker/)
##################################################

# FROM: setting the base image (here: image from openjdk, tag: 8-jdk-alpine)
# BaseImage Dockerfile: (https://github.com/docker-library/openjdk/blob/master/8/jdk/alpine/Dockerfile)
FROM openjdk:8-jdk-alpine

# LABEL: set a label to provide some metadata to the image
LABEL maintainer="felix falkenberg"
LABEL version="0.0.1"
LABEL describtion="labor app docker image"
LABEL labor_start_date="20181204"

# VOLUME: creates a mount point, where later SpringBoot can create working directories e.g. for Tomcat
VOLUME /tmp

# ARG: defines a variable (later referenced with ${VariableName}), which comes in during command "docker build"
# REMARK: just deactivated, to keep it for labor purposes very very tiny and w/o frils
# ARG JAR_FILE

# EXPOSE: defines on which port the docker container shall listen on runtime
EXPOSE 80/tcp

# COPY: copies new files from SOURCE (1.argument) under DESTINATION (2.argument) within the image
# WARNING: the SOURCE here is hard coded, better is to do it via ARG variables, which come in with docker build cmd
COPY ./target/app-0.0.1.jar app.jar

# ENTRYPOINT: (in exec form: executeable, param1, param2) allows to run a docker container as executeable
# with docker run command. The Entrypoint executes the app to run it..
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]

# EOF