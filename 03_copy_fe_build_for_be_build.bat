:: msdos cli help (https://computerhope.com/msdos.htm) / :: (https://computerhope.com/issues/ch001676.htm)

:: go into the dir of the frontend app (here: react-app)
:: SPRINGBOOT cd .\src\main\ui
:: PLAY cd .\ui
cd .\src\main\ui

:: SPRINGBOOT robocopy .\dist ..\resources\static /S
:: PLAY .\dist ..\public\ui /S
robocopy .\dist\ui ..\resources\static /S

:: SPRINGBOOT cd ..\resources\static
:: PLAY cd ..\public\ui
cd ..\resources\static
dir

::pause