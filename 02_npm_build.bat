:: msdos cli help (https://computerhope.com/msdos.htm) / :: (https://computerhope.com/issues/ch001676.htm)

:: go into the dir of the frontend app (here: react-app)
:: SPRINGBOOT cd .\src\main\ui
:: PLAY cd .\ui
cd .\src\main\ui

:: execute the NPM build command to create the production build
:: mostly provided in the subdir /build
npm run build