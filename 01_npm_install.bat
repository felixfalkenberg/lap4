:: msdos cli help (https://computerhope.com/msdos.htm) / :: (https://computerhope.com/issues/ch001676.htm)

:: go into the dir of the frontend app (here: react-app)
:: SPRINGBOOT cd .\src\main\ui
:: PLAY cd .\ui
cd .\src\main\ui

:: execute the YARN install command to build the app 
:: resp. to get all dependencies
npm install