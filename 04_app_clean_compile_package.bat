:: msdos cli help (https://computerhope.com/msdos.htm) / :: (https://computerhope.com/issues/ch001676.htm)

:: stay in the dir, where the pom.xml is contained
cd .

:: execute the MVN clean package command to build the app 
:: into a self-contained jar-file
:: SPRINGBOOT: mvn clean package
:: PLAY: sbt clean package

:: SBT command sheet (https://www.scala-sbt.org/1.x/docs/Running.html#Common+commands)
:: SBT full command list (https://www.scala-sbt.org/1.x/docs/Command-Line-Reference.html)
mvn clean package