# lap4
labor app for [DOCKER](https://hub.docker.com/r/felixfalkenberg/lap4/) with SpringBoot, angular.io, NPM/ng and old good old MS-Batch-Scripts

# credits to the giants...
## Docker stuff related
- (https://spring.io/guides/gs/spring-boot-docker/)
- (https://www.baeldung.com/dockerizing-spring-boot-application)
- (http://blog.shippable.com/build-a-docker-image-and-push-it-to-docker-hub)
## BE stuff related
- (https://blog.squeng.com/2017/08/13/werkzeuge/)
- (https://www.baeldung.com)
- (https://github.com/eugenp/tutorials)
- (https://www.spring.io)
- (https://www.github.com/spring-guides)
## FE stuff related
- (https://www.angular.io) | (https://cli.angular.io/)
- (https://developer.okta.com/blog/2018/08/22/basic-crud-angular-7-and-spring-boot-2)
- (https://github.com/oktadeveloper/okta-spring-boot-2-angular-7-example)
- (https://stackoverflow.com/questions/tagged/spring)
- (https://stackblitz.com/angular) | (https://angular-lap3-ffb01.stackblitz.io)

# help for writing .md (markdown) styled texts in GitHub etc
- (https://guides.github.com/features/mastering-markdown/#syntax)

# getting started with docker on windows 10 (but no in upper virtual environment)
- so switching to an real PC and switching the BIOS to provide Hardware Virtualization like (https://docs.docker.com/docker-for-windows/install/)
- Install Docker CE (for Windows) from (https://docs.docker.com/docker-for-windows/install/)
- Solving issue **"error-get-https-registry-1-docker-io-v2-net-http-request-canceled"** like (https://github.com/moby/moby/issues/22635) trying to Simply set the default DNS to 8.8.8.8 on the "vEthernet (DockerNAT)" network adapter in Win10 like (https://windowsreport.com/dns-server-1-1-1-1/)... later restart system and tried again sucessfully with 
- Perform the steps in Get Started Guide (https://docs.docker.com/get-started/)
- Lern from the Dockerfile Best Practices (https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
- Docker CLI reference (https://docs.docker.com/engine/reference/commandline/docker/)
- Dockerfile reference (https://docs.docker.com/engine/reference/builder/)
- Minimal steps to push a docker image to dockerhub (http://blog.shippable.com/build-a-docker-image-and-push-it-to-docker-hub)

# minimal docker build environment (under win10)
- **JAVA**
  - Install Amazon Corretto from (https://docs.aws.amazon.com/corretto/latest/corretto-8-ug/windows-7-install.html) as Java JDK/JRE Amazon Corretto 8 in Win10 under (C:/programme/DEV/java/Amazon Corretto/)
  - Set Environment Variable JAVA_HOME to "C:/programme/DEV/java/Amazon Corretto/jdk1.8.0_192" and add %JAVA_HOME%\bin to PATH Environment Variable
  - Restart system

- **MAVEN**
  - Download and Extract Maven from (https://maven.apache.org/download.cgi) in Win10 under (C:/programme/DEV/apache-maven-#.#.#/)
  - Set Environment Variable MAVEN_HOME to "C:/programme/DEV/apache-maven-#.#.#" and add %MAVEN_BIN%\bin to PATH Environment Variable
  - Restart System

- **GIT**
  - Download and Install Git for Windows from (https://gitforwindows.org/) OR (httsp://git-scm/download/win) in Win10 under (C:/programme/DEV/Git/)
  - Setup Git with User Name and User Email like (https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)
  
- **NPM** and **Node.js**
  - Download **Node.js** and (embedded) **NPM** from (https://nodejs.org/en) and install it in Win 10 under (C:/programme/DEV/nodejs/)
  - Restart System 
  - start cmd.exe and type `node --version` to verify, if **node.js** is active
  - in cmd.exe type `npm --version` to veriy, if **NPM** is active
  
- **YARN** 
  - Download **YARN** from (https://yarnpkg.com/en/docs/install) and install it in Win10 under (C:/programme/DEV/yarn/)
  - Restart System
  - start cmd.exe and type `yarn --version` to verify, if **yarn** is active  
  
  
# Lifecyle steps from with docker images and docker containers via docker CLI:
- building the related docker image under name **lap4_di** and **mind the . dot**:
> `docker build --tag=lap4_di --rm=true .`

- running the newly minted docker image **lap4_di** as container under name **lap4_dc**:
> `docker run --name=lap4_dc --publish=8080:8080 lap4_di`
- start viewing results in browser via:
> `(https://localhost:8080/)`

- show the (running) docker containers:
> `docker container ls -a`
  **OR**
> `docker ps -a`
- show the (present) docker images:
> `docker image ls -a`
- get some info about a docker object (i.e. image, container, ...):
> `docker inspect lap4_dc` | `lap4_di`
**OR**
> `docker container inspect lap4_dc`
**OR**
> `docker image inspect lap4_di`

- save a docker image (incl. its parent layers etc) into a local tar-file (for later import via *docker load --input lap4_di.tar*)
> `docker save --output lap4_di.tar lap4_di`

- prepare a docker image (w/i docker context as current dir .) for later push it to the account at dockerhub
> `docker build --tag=felixfalkenberg/lap4:latest --rm=true .`
**AND THEN**
> `docker push felixfalkenberg/lap4:latest`

- pull a docker image with name **felixfalkenberg/lap4:latest** from dockerhub (https://hub.docker.com/r/felixfalkenberg/lap4/) 
> `docker pull felixfalkenberg/lap4:latest`
**AND THEN / OR**
> `docker run --name=lap4_dc --publish=8080:8080 felixfalkenberg/lap4:latest`

- stop the running docker container **lap4_dc** (which later then can started again via *docker start **lap4_dc** *):
> `docker stop lap4_dc`
  **OR**
> `docker kill lap4_dc`

- remove a *unused* container with name **lap4_dc** **OR** **all** unused via docker CLI:
> `docker rm lap4_dc -f` **OR** `docker container prune --force`
- remove a *unused* image with name **lap4_di** **OR** **all** unused via docker CLI:
> `docker rmi lap4_di -f` **OR** `docker image prune  --all --force`

# regular dev and build environment (java, angular.io)
- Languages: **Java, Angular.io**
- Labor: VDI-Win10 by **Shadow.tech** (https://shadow.tech/usen/)
- JDK: **Amazon Correto 8** [based upon openJDK 8] (https://aws.amazon.com/de/corretto/) 
- IDE = VSC **Visual Studio Code** (https://code.visualstudio.com/) (https://code.visualstudio.com/docs)
- Git = **GitHub**
- Docker Repo = **DockerHub** (https://hub.docker.com/r/felixfalkenberg/lap4/) - currently private until ready
- Java Build = **Maven** (https://maven.apache.org)
- Angular.io Create = **angular cli** (https://angular.io/cli) (https://material.angular.io/)
- Angular.io Build = **npm** (https://docs.npmjs.com/)
- Shell: **cmd.exe** / powershell.exe / .bat-files (https://www.computerhope.com/msdos.htm) (https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/windows-commands)
- CLI Browser = **httpie** [for windows] (https://httpie.org/doc), installed via **pip** (https://pip.pypa.io/en/stable/installing/), got via (https://www.python.org/downloads/windows/)
- API Docu (Backend) = **Swagger** (http://localhost:8080/swagger-ui.html)
- Browser = **chrome** (https://www.google.de/chrome/)
- Target System => **FE** (http://localhost:8080/) | **BE** (http://localhost:8080/api/)  ... only for **DEMO** / "LABOR" mode

# API related
- when running, use (**https://localhost:8080/swagger-ui.html**) to see the **Swagger-UI** resp OAS2.0/3.0 API documentation (interactively)

# DevOps / Health related (minimistic)
- when running (as JAR or docker container) type (**https://localhost:8080/actuator/health**) to get a health signal from the SpringBoot-App

## ...
