:: msdos cli help (https://computerhope.com/msdos.htm) / :: (https://computerhope.com/issues/ch001676.htm)

:: go into the dir of the frontend app (here: react-app)
:: SPRINGBOOT cd .\src\main\ui
:: PLAY cd .\ui
cd .\src\main\ui

:: delete the "build" folder completely
:: to force later a really fresh YARN build with "yarn install"
rd /S /Q .\dist

:: go up to "src"
:: SPRINGBOOT cd ..\..\
:: PLAY cd ..
cd ..\..\
dir
:: pause

:: SPRINGBOOT: go down to "main/resources/static" to delete there the former COPIED YARN-NPM build artifacts
:: PLAY: go down to ".\public\ui\" to delete there the former COPIED YARN-NPM build artifacts
:: SPRINGBOOT cd .\main\resources\static
:: PLAY cd .\public\ui\
cd .\main\resources\static
dir
:: pause

del /S /F /Q .\*.*
rd /S /Q .\static

:: END 
:: pause