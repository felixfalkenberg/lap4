package demo;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class SimpleTest 
{
    /**
     * needed to tweak maven, but not a real test...
     */
    @Test
    public void neverFails()
    {
        assertTrue( true );
    }
}
