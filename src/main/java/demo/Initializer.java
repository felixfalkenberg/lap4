package demo;

import demo.model.Item;
import demo.model.ItemRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.stream.Stream;

@Component
class Initializer implements CommandLineRunner {

    private final ItemRepository repository;

    @Autowired
    public Initializer(ItemRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... strings) {

        //Create some Items via a Stream and fast method
        Stream.of(
            "Apple", 
            "Cherry", 
            "Banana",
            "Mango"
        )
        .forEach(
            name -> repository.save(new Item(name))
        );

        //now read them all and show them on the console
        repository.findAll().forEach(System.out::println);
    }
}