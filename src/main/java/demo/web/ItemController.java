package demo.web;

import demo.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/api") //181130 FFB: again activated, but keep in sync with (ui/src/app/shared/item/item.service.ts)
@CrossOrigin(origins = "http://localhost:4200")
class ItemController {

    private final Logger log = LoggerFactory.getLogger(ItemController.class);
    private ItemRepository itemRepository;

    public ItemController(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @GetMapping("/items")
    Collection<Item> items() {
        return (Collection<Item>) itemRepository.findAll();
    }

    @GetMapping("/item/{id}")
    ResponseEntity<?> getItem(@PathVariable Long id) {

        Optional<Item> Item = itemRepository.findById(id);

        return Item
                .map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/item")
    ResponseEntity<Item> createItem(@Valid @RequestBody Item Item) throws URISyntaxException {
        log.info("Request to create Item: {}", Item);

        Item result = itemRepository.save(Item);

        return ResponseEntity
                .created(new URI("/api/item/" + result.getId()))
                .body(result);
    }

    @PostMapping("/item/{id}")
    ResponseEntity<Item> updateItem(@PathVariable Long id, @Valid @RequestBody Item Item) {

        Item.setId(id);
        log.info("Request to update Item: {}", Item);

        Item result = itemRepository.save(Item);

        return ResponseEntity
                .ok()
                .body(result);
    }

    @DeleteMapping("/item/{id}")
    public ResponseEntity<?> deleteItem(@PathVariable Long id) {

        log.info("Request to delete Item: {}", id);

        itemRepository.deleteById(id);

        return ResponseEntity
                .ok()
                .build();
    }
}