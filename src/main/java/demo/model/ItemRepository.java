package demo.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")
public interface ItemRepository extends CrudRepository<Item, Long>{
   Item findByName(String name); //custom method, which will be implemented by Spring itself (magically)
}