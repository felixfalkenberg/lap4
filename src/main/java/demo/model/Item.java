package demo.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "generic_item")
public class Item {

    @Id
    @GeneratedValue
    private Long id; //PK
    @NonNull
    private String name;
    private String info;
    private String value;
}