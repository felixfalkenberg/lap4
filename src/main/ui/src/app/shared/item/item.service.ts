import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ItemEditComponent } from 'src/app/item-edit/item-edit.component';
import { JsonPipe } from '@angular/common';

@Injectable({ providedIn: 'root' })
export class ItemService {

  //fields
  //public API = '//localhost:8080/api'; //181130 FFB: currently deactivated
  public API = '//localhost:8080/api';
  public ITEM_API = this.API + '/item';

  //constructor
  constructor(private http: HttpClient) { }

  //get all items
  getAll(): Observable<any> {
    return this.http.get(this.API + '/items');
  }

  //get 1 item
  get(id: string) {
    return this.http.get(this.ITEM_API + '/' + id);
  }

  //save 1 item or create 1 item
  save(item: any): Observable<any> {
    let result: Observable<Object>;

    //print out the item to be saved
    console.info("FFB item: " + JSON.stringify(item));

    if (item['id']) {
      result = this.http.post(this.ITEM_API + '/' + item.id, item); //update 1 item: TRY: PUT / PATCH
    } else {
      result = this.http.post(this.ITEM_API, item); //create 1 item
    }

    return result;
  }

  //delete 1 item
  remove(id: any) {
    return this.http.delete(this.ITEM_API + '/' + id);
  }
}
