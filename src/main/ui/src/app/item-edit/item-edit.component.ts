//global imports
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';

//local imports
import { ItemService } from '../shared/item/item.service';

@Component({
  selector: 'app-item-edit',
  templateUrl: './item-edit.component.html',
  styleUrls: ['./item-edit.component.css']
})
export class ItemEditComponent implements OnInit, OnDestroy {

  //fields
  item: any = {};
  sub: Subscription;

  //constructor
  constructor(  private route: ActivatedRoute,
                private router: Router,
                private itemService: ItemService ) { 
  }

  //what shall happen on init of this component
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {

      //get the parameter "id" from params of the route
      const id = params['id'];

      //was there a parameter "id"
      if (id) {

        //make a lookup via ItemService to get the item object for this "id"
        this.itemService.get(id).subscribe((item: any) => {

          //if a item object was found, get out the details
          if (item) {
            
            this.item = item;
            //populate back the id of the item
            //this.item.id = id;

            //this.item.href = item._links.self.href;
            //log the href path of item
            console.info("FFB item: " + JSON.stringify(item));

          } else {
            //nothing for this id was found, so write a log entry and go back to the item list
            console.log(`Item with id '${id}' not found, returning to list`);
            this.gotoList();
          }

        });
      }
    });
  }

  //what shall happen on destroy of this components
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  //method for navigation back to list of items
  gotoList() {
    this.router.navigate(['/item-list']);
  }

  //method for save => POST item ( /item/{id} | /item )
  save(form: NgForm) {
    this.itemService.save(form).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

  //method for remove => DELETE item
  remove(id) {
    this.itemService.remove(id).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

}
