// imports
import { Component, OnInit } from '@angular/core';

// local imports
import { ItemService } from '../shared/item/item.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {
  items: Array<any>; //define the list of items as array

  constructor(private itemService: ItemService ) { }

  //do what shall happen on init
  ngOnInit() {
    this.itemService.getAll().subscribe( data => {
      this.items = data; //take the data and put it into predefined items
    });
  }

}
