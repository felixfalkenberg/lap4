// imports
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule, MatCardModule, MatInputModule, MatListModule, MatToolbarModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule } from '@angular/forms';

// local imports
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemListComponent } from './item-list/item-list.component';
import { HomeComponent } from './home/home.component';
import { ItemEditComponent } from './item-edit/item-edit.component';

// specify the modules
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ItemEditComponent,
    ItemListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, //user defined local routing of paths
    HttpClientModule,
    BrowserAnimationsModule,

    //modules of material design
    MatButtonModule, 
    MatCardModule,
    MatInputModule, 
    MatListModule, 
    MatToolbarModule,

    //Forms
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
